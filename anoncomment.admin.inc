<?php

/**
 * @file
 * Admin page callbacks for the anoncomment module. Largely copied from comment module's comment.admin.inc
 */


//get the functions we need from the main comment module
require_once( drupal_get_path('module', 'comment') . "/comment.admin.inc");


/**
 * Menu callback; present an administrative comment listing.
 */
function anoncomment_admin($type = 'anon') {
  $edit = $_POST;

  if (isset($edit['operation']) && ($edit['operation'] == 'delete') && isset($edit['comments']) && $edit['comments']) {
    return drupal_get_form('comment_multiple_delete_confirm');
  }
  else {
    return drupal_get_form('anoncomment_admin_overview');
  }
}


/**
 * Form builder; Builds the comment overview form for the admin.
 *
 * @return
 *   The form structure.
 * @ingroup forms
 * @see comment_admin_overview_validate()
 * @see comment_admin_overview_submit()
 * @see theme_comment_admin_overview()
 */
function anoncomment_admin_overview() {
  // build an 'Update options' form
  $form['options'] = array(
    '#type' => 'fieldset', '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">', '#suffix' => '</div>'
  );
  $options = array(
      'unpublish' => t('Unpublish the selected comments'),
      'modify' => t('De-anonymize the selected comments'),
      'delete' => t('Delete the selected comments'),
    );

  $form['options']['operation'] = array('#type' => 'select', '#options' => $options, '#default_value' => 'publish');
  $form['options']['submit'] = array('#type' => 'submit', '#value' => t('Update'));

  // load the comments that we want to display
  $form['header'] = array('#type' => 'value', '#value' => array(
    theme('table_select_header_cell'),
    array('data' => t('Subject'), 'field' => 'subject'),
    array('data' => t('Actual Author'), 'field' => 'name'),
    array('data' => t('Posted in'), 'field' => 'node_title'),
    array('data' => t('Time'), 'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Operations'))
  ));
  $result = pager_query('SELECT c.subject, c.nid, c.cid, c.comment, c.timestamp, c.status, c.name, c.homepage, u.name AS registered_name, u.uid, n.title as node_title FROM {comments} c
  INNER JOIN {anoncomment_authors} a ON c.cid = a.cid
  INNER JOIN {users} u ON u.uid = a.uid
  INNER JOIN {node} n ON n.nid = c.nid WHERE c.uid = 0 AND c.status = %d'. tablesort_sql($form['header']['#value']), 50, 0, NULL, $status);

  // build a table listing the appropriate comments
  $destination = drupal_get_destination();
  while ($comment = db_fetch_object($result)) {
    $comments[$comment->cid] = '';
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    $form['subject'][$comment->cid] = array('#value' => l($comment->subject, 'node/'. $comment->nid, array('attributes' => array('title' => truncate_utf8($comment->comment, 128)), 'fragment' => 'comment-'. $comment->cid)));
    $form['username'][$comment->cid] = array('#value' => theme('username', $comment));
    $form['node_title'][$comment->cid] = array('#value' => l($comment->node_title, 'node/'. $comment->nid));
    $form['timestamp'][$comment->cid] = array('#value' => format_date($comment->timestamp, 'small'));
    $form['operations'][$comment->cid] = array('#value' => l(t('edit'), 'comment/edit/'. $comment->cid, array('query' => $destination)));
  }
  $form['comments'] = array('#type' => 'checkboxes', '#options' => isset($comments) ? $comments: array());
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}


/**
 * Validate anoncomment_admin_overview form submissions.
 *
 * We can't execute any 'Update options' if no comments were selected.
 */
function anoncomment_admin_overview_validate($form, &$form_state) {
  $form_state['values']['comments'] = array_diff($form_state['values']['comments'], array(0));
  if (count($form_state['values']['comments']) == 0) {
    form_set_error('', t('Please select one or more anonymized comments to perform the update on.'));
  }
}


/**
 * Process anoncomment_admin_overview form submissions.
 *
 * Execute the chosen 'Update option' on the selected comments, such as
 * publishing, unpublishing or deleting. Would be nice to add function to make public....
 */
function anoncomment_admin_overview_submit($form, &$form_state) {
  //$operations = comment_operations();

  if (!empty($form_state['values']['operation'])) {
    $operation = $form_state['values']['operation'];
    //perform the operation
    switch ($operation) {
      case "unpublish":
        foreach ($form_state['values']['comments'] as $cid => $value) {
          if ($value) {
            db_query('UPDATE {comments} SET status = %d WHERE cid = %d', COMMENT_NOT_PUBLISHED, $cid);
          }
        }
        break;
      case "modify":
        /* we're de-anonymizing, which is trickier
           but we choose to leave the entry in anoncomment_authors so there's a record of the
           comment having been initally posted anonymously (just in case) */
        foreach ($form_state['values']['comments'] as $cid => $value) {
          if ($value) {
            db_query('UPDATE {comments} c SET
                        c.uid = (SELECT aca.uid FROM {anoncomment_authors} aca WHERE aca.cid = c.cid),
                        c.name = (SELECT u.name FROM {users} u
                            INNER JOIN {anoncomment_authors} aca ON aca.uid = u.uid WHERE aca.cid = c.cid)
                        WHERE EXISTS (SELECT aca.uid FROM {anoncomment_authors} aca WHERE aca.cid = c.cid)
                        AND EXISTS (SELECT u.name FROM {users} u
                            INNER JOIN {anoncomment_authors} aca ON aca.uid = u.uid WHERE aca.cid = c.cid)
                        AND c.cid = %d', $cid);
          }
        }
        break;
      case "delete":
        // we shouldn't ever wind up here - handled by comment_multiple_delete_submit()
        cache_clear_all();
        drupal_set_message(t('An error occurred in the anoncomment module and the comments could not be deleted.'));
        return;
        break;
    }
    //refresh node statistics (not strictly necessary for 'modify' but whatever)
    $comment = _comment_load($cid);
    _comment_update_node_statistics($comment->nid);
    // Allow modules to respond to the updating of a comment.
    comment_invoke_comment($comment, $form_state['values']['operation']);
    // Add an entry to the watchdog log.
    watchdog('content', 'Comment: updated %subject.', array('%subject' => $comment->subject), WATCHDOG_NOTICE, l(t('view'), 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)));

    cache_clear_all();
    drupal_set_message(t('The update has been performed.'));
    //$form_state['redirect'] = 'admin/content/comment';
  }
}


/**
 * Theme the comment admin form.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @ingroup themeable
 */
function theme_anoncomment_admin_overview($form) {
  $output = drupal_render($form['options']);
  if (isset($form['subject']) && is_array($form['subject'])) {
    foreach (element_children($form['subject']) as $key) {
      $row = array();
      $row[] = drupal_render($form['comments'][$key]);
      $row[] = drupal_render($form['subject'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      $row[] = drupal_render($form['node_title'][$key]);
      $row[] = drupal_render($form['timestamp'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No anonymized comments presently exist.'), 'colspan' => '6'));
  }
  $output .= theme('table', $form['header']['#value'], $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  return $output;
}